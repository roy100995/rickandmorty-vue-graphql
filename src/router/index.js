import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/episodes/:page?',
    alias: '/',
    name: 'Home',
    component: () =>
      import(/* webpackChunkNAme: "episodes" */ '../views/Episodes.vue')
  },
  {
    path: '/locations/:page?',
    name: 'Locations',
    component: () =>
      import(/* webpackChunkNAme: "locations" */ '../views/Locations.vue')
  },
  {
    path: '/characters/:page?',
    alias: '/',
    name: 'Characters',
    component: () =>
      import(/* webpackChunkNAme: "characters" */ '../views/Characters.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
